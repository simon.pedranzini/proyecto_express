const mongoose = require('mongoose');
const path = require('path')
// LOCAL - MONGODB
const DB_URL = 'mongodb://localhost:27017/06project-express';
// CLOUD - MONGO ATLAS
// const password = 'PhppQ8nCja8K2KW'
// const db_name = 'proyecto_node-peliculas'
// const DB_URL = `mongodb+srv://simonpedran:${password}@cluster0.ctd7p.mongodb.net/${db_name}?retryWrites=true&w=majority`;

const CONFIG_DB = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

const connectDb = async () => {
    try {
        const conexionDb = await mongoose.connect(DB_URL, CONFIG_DB);
        console.log(`Conectado a la DB successfully.`)
    } catch {
        return res.status(500).sendFile(path.join(__dirname, '../error500.html'))
    }
}

module.exports = { DB_URL, CONFIG_DB, connectDb };