const express = require('express');
const Cinemas = require('../models/Cinema');

const router = express.Router();

router.put('/add-pelicula', async (req, res, next) => {
    try {
        const { cinemaId } = req.body;
        const { movieId } = req.body;
        // const { cinemaId, movieId } = req.body;
        const cinemaUpdated = await Cinemas.findByIdAndUpdate(cinemaId,
            // { $push: { movies : movieId } }, // el push te mete los que pidas, aún que ya existan.
            { $addToSet: { movies : movieId }}, // el addToSet agrega solo artículos único.
            { new: true }
            );
        return res.status(200).json(cinemaUpdated);
    } catch (error) {
        return next(error);
    }
})

module.exports = router;