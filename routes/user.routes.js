const express = require('express');
const User = require('../models/User');
const passport = require('passport');
const session = require('express-session');

const router = express.Router();

router.post('/register', (req, res, next) => {
    //    return res.status(200).json('Web registro OK.')
    try {
        const done = (error, savedUser) => {
            if (error) {
                return next(error);
            }
            return res.status(201).json(savedUser) // ERROR 201, created!
        };
        passport.authenticate('registrito', done)(req);
    } catch (error) {
        return next(error)
    }
});

router.post('/', async (req, res, next) => {
    try {} catch (error) {
        return next(error);
    }
})

router.post('/acceso', (req, res, next) => {
 passport.authenticate('login', (error, user) => {
        if (error) return next(error)
         req.logIn(user, (error) => {
            // si hay un error durante el logeo, devuele error
            if (error) {
                return next(error);
            }
            // si no hay errror, se devuelve el usuario logueado
            return res.status(200).json(user)
        });
    })(req)
});


router.post('/logout', (req, res, next) => {
    if (req.user) {
        //deslogueo al usuario

        req.logout();
        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.status(200).json('Usuario desloqueado');
        })
    } else {
        const error = new Error('No existe usuario logueado');
        return next(error)
    }
});

module.exports = router;