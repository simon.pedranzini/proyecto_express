const express = require('express');
const { Mongoose } = require('mongoose');
const Cinemas = require('../models/Cinema');

const router = express.Router();

router.post('/construir', async (req, res, next) => {
    try {
        const newCinema = new Cinemas({
            name: req.body.name,
            location: req.body.location,
            movies: []
        });

        const createdCinema = await newCinema.save();
        console.log(`El cine "${createdCinema.name}" se ha creado correctamente!`);
        return res.status(200).json(createdCinema);
    } catch (error) {
        return next(error);
    }
})

module.exports = router;
