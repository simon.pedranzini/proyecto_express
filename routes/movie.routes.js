const express = require('express');
const Movies = require('../models/Movie')

const router = express.Router();

router.get('/', async (req, res) => { // NO SE PONE /peliculas, por que ya está puesto en el index.js
    try {
        const movies = await Movies.find();
        return res.send(movies);
    } catch (error) {
        return res.status(404).json(`Ha habido un error en la petición: ${error}`)
    }
});

router.get('/:id', async (req, res) => {
    try {
        const peliculaId = req.params.id;
        const pelicula = await Movies.findById(peliculaId);

        if (pelicula) {
            return res.status(200).json(pelicula)
        } else {
            return res.status(404).json(`Pelicula no encontrada con el id ${id}, prueba uno diferente!`)
        }
    } catch (error) {
        return res.status(500).json(error)
    }
});


router.get('/titulo/:alias', async (req, res) => {
    try {
        const peliculaTitulo = req.params.alias;
        const pelicula = await Movies.find({
            title: {
                $regex: new RegExp("^" + peliculaTitulo.toLowerCase(), "i") // trasnforma todo el parametro que se pase por URL a minuscula para evitar fallos en la busqueda.
            },
        });
        if (pelicula) {
            return res.status(200).json(pelicula)
        } else {
            next(error);
            // return res.status(404).json(`No se ha encontrado pelicula con dicho titulo. Revisa nuevamente el listado.`)
        }
    } catch (error) {
        return res.status(500).json(error)
    }
});

router.get('/puntuacion/:valor', async (req, res, next) => {
    try {
        const { valor } = req.params; // Esto es lo mismo que hacer const edad = req.params.release_date;
        const pelicula = await Movies.find({
            puntuacion : { $gte: valor }
        });
        console.log(`Peliculas con puntuacion mayor a: ${valor}`)
        return res.status(200).json(pelicula)
    } catch (error) {
        return next(error);
        // return res.status(500).json(error)
    }
});

router.get('/estreno/:fecha', async (req, res, next) => {
    try {
        const { fecha } = req.params;
        const pelicula = await Movies.find({
            release_date : { $gte: fecha }
        });
        console.log(`Peliculas con fecha de estreno posteriores a ${fecha}`);
        return res.status(200).json(pelicula)
    } catch (error) {
        return next(error);
    }
});


module.exports = router;