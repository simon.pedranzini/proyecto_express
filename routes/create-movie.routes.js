const express = require('express');
const Movies = require('../models/Movie')
const { upload } = require('../middlewares/file.middleware');

const router = express.Router();

router.post('/creacion', [upload.single('nombre-del-campo')], async (req, res, next) => { // La subida de imagenes en postman siempre en Body > form-data
    try {
        console.log('req.file', req.file)
        const moviePoster = req.file ? req.file.filename : null; // ternario, si tengo imagen, le guardo el filename sino le pongo null.

        const newMovie = new Movies({
                id: req.body.id,
                overview: req.body.overview,
                puntuacion: req.body.puntuacion,
                release_date: req.body.release_date,
                title: req.body.title,
                image: moviePoster,
        });

        const createdMovie = await newMovie.save()
        console.log(`La película "${createdMovie.title}" se ha añadido correctamente!!`);
        // SOLID PRINCIPLES -> S = Single Responsability Principale
        return res.status(200).json(createdMovie)

    } catch (error) {
        return next(error);
    }
});

module.exports = router;