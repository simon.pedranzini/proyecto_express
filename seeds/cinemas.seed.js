const mongoose = require('mongoose');
const { CONFIG_DB, DB_URL } = require('../config/db');
const Cinemas = require('../models/Cinema')



const cinemasArray = [
    {
        "name": "Cines Simón",
        "location": "Barcelona",
    },
    {
        "name": "Cines José",
        "location": "Malaga",
    },
    {
        "name": "Cines Aquiles",
        "location": "Guadalajara",
    },
    {
        "name": "Cines Hector",
        "location": "Logroño",
    },
    {
        "name": "Cines Lucas",
        "location": "Tenerife",
    },
];

mongoose.connect(DB_URL, CONFIG_DB)
    .then(async () => {
        //Ejecutar seed para cargar los cines
        const allCinemas = await Cinemas.find();
        // Si ya hay cines, los borramos
        if (allCinemas.length) {
            await Cinemas.collection.drop();
            console.log(`Cines borrados!`)
        }
    }).catch (error => console.log(`Error buscando en la base de datos ${error}`))
        .then(async () => {
            //Añadir los cines del seed a la BBDD
            await Cinemas.insertMany(cinemasArray);
            console.log(`Cines añadidos correctamente!`)
        }).catch (error => console.log(`Error añadiendo los nuevos datos ${error}`))
            .finally(() => mongoose.disconnect(console.log(`Cerrando conexión. Adiós!`)))