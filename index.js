const express = require('express');
const { connectDb, DB_URL } = require('./config/db'); // Se importa para hacer la conexión a la BBDD
const passport = require('passport');
require('./auth/register.strategy'); // precargar la estrategia para poder utilizarla luego
require('./auth/login.strategy') // precargar el login para poder utilizarla luego
const session = require('express-session');
const MongoStore = require('connect-mongo');

// ROUTES COLLECTIONS
const routes = require('./routes/index.routes'); // Se importa para hacer la ruta al INDEX
const movieRouter = require('./routes/movie.routes');
const cinemaRouter = require('./routes/cinema.routes');
const userRouter = require('./routes/user.routes');

// ROUTES CRUD
const createMovieRouter = require('./routes/create-movie.routes');
const deteleMovieRouter = require('./routes/delete-movie.routes');
const modifyMovieRouter = require('./routes/modify-movie.routes');
const createCinemaRouter = require('./routes/create-cinema.routes');
const addMovieToCinema = require('./routes/add-movie-cinema.routes');

connectDb(); // Conexión a la BBDD

const PORT = 3000;
const server = express();

server.use(
    session({
      secret: 'upgradehub_node', // ¡Este secreto tendremos que cambiarlo en producción!
      resave: false, // Solo guardará la sesión si hay cambios en ella.
      saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
      cookie: {
        maxAge: 3600000, // Milisegundos de duración de nuestra cookie, en este caso será una hora.
      },
      store: MongoStore.create({ mongoUrl: DB_URL })
    })
  );
server.use(passport.initialize());
server.use(passport.session());


// Esta línea, le dice a express que si recibe un POST, PUT con JSON adjunto,
// lo lea y lo mande al endpoint dentro del req.body
server.use(express.json());
// si viene en POST,PUT y viene ne formato form-urlencodde, meta la informacion en req.body
server.use(express.urlencoded({ extended : false }));
server.use(express.static(__dirname + '/public')); // Para los estilos CSS
// server.use(passport.initialize()); // Middleware para inicializar passport en nuestro servidor
// server.use(passport.session());


server.use('/', routes); // pagina inicial
server.use('/peliculas', movieRouter); // show peliculas
server.use('/pelicula', createMovieRouter);
server.use('/pelicula', deteleMovieRouter);
server.use('/pelicula', modifyMovieRouter);
server.use('/cines', cinemaRouter); // Show cines
server.use('/cines', createCinemaRouter); // Crear un cine
server.use('/cine', addMovieToCinema); //Añadir pelicula a un cine
server.use('/auth', userRouter); // registro del usuario en BD


// Controlador de ERrores (Error de habdler) Siempre debajo de las rutas
server.use((error, req, res, next ) => { // NEXT PARTE MAS IMPORTANTE DE EXPRESS
    // error.mesage = 500
    //error.status = 'Unexpected Error';
    // al usar next(), la ejecución llega a este punto.
    // si no se usa next(), no se llega hasta aquí.
    const status = error.status || 500;
    const message = error.message || 'Unexpected error!';

    return res.status(status).json(message);
});


/*
CRUD (acsróstico)
Create
    Read
        Update
            Delete
*/

const callbackServidor = () =>  {
    console.log(`El servidor está arrancado en la dirección http://localhost:${PORT}`);
}

server.listen(PORT, callbackServidor);

module.exports = PORT;