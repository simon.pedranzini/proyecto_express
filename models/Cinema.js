const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cinemaSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    movies: [{
        // EL REF ES EL NOMBRE DEL MODELO AL CUAL QUEREMOS HACER REFERENCIA.
        type: mongoose.Types.ObjectId,
        ref: 'Movies'
    }]
}, {
    timestamps: true,
});

// Crear el modelo y exportarlo
const Cinemas = mongoose.model('Cinemas', cinemaSchema);
module.exports = Cinemas;
