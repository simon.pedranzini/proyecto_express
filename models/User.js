const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userShema = new Schema({
    //email y password en vuestro servidor
    email: { type: String, require: true},
    password: { type: String, required: true},
    name: { type: String }
},
 { timestamps: true }
);

const User = mongoose.model('Users', userShema);

module.exports = User;