const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Creamos el esquema que tendrán nuestras películas
const movieSchema = new Schema (
    {
    title: { type: String },
    puntuacion: { type: Number },
    overview : { type: String },
    release_date: { type: Number },
    id: { type: Number },
    image: { type: String }
},
{
    timestamps: true,
});

// Creamos el modelo y lo exportamos
const Movies = mongoose.model('Movies', movieSchema);
module.exports = Movies;
