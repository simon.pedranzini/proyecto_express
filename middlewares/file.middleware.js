// Paquete MULTER => npm install multer

const path = require('path');
const fs = require('fs');
const multer = require('multer');

const storage = multer.diskStorage({
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}-${file.originalname}`);
    },
    destination: (req, file, callback) => {
        const directory = path.join(__dirname, '../public/uploads'); //dirname constante que proporciona path
        callback(null, directory);
    },
});

const ACCEPTED_FILE_EXTENSIONS = ['image/png', 'image/jpg', 'image/jpeg'];

const fileFilter = (req, file, callback) => {

    // 1. Si el tipo de archivo no coincide, pasamos error.
    if (ACCEPTED_FILE_EXTENSIONS.includes(file.mimetype)) {
        //es ok, mimetype valido
        callback(null, true);
    } else {
        //no es mimetype válido
        const error = new Error('TypeMime inválido');
        error.status = 400; // Bad request
        callback(error, null);
    };
}

const upload = multer({
    storage,
    fileFilter,
});

module.exports = { upload }