const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const bcrypt = require('bcrypt')


const loginStrategy =  new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {

        try {
        // 1-Comprobar que el email sea válido


        // 2-Comprobar que el usuario existe en nuestra DB
        const currentUser = await User.findOne({ email : email });
        // Si no existe =>
        if (!currentUser) {
            const error = new Error('El usuario no existe!');
            return done(error);
        }
        // 3-Comprobar si las pass que intruduce es la misma que tiene el usuario
        const isValidPassword = await bcrypt.compare(password, currentUser.password);

        // 4-Si el usuario existe, comprobamos que la pass esté correcta
        if (!isValidPassword) {
            const error = new Error(`La combinación de usuario y contraseña no es válida.`);
            return done(error);
        }
        // Si todo es OK, se elimina la pass del usuario que devuelve la DB y se completa el callback con el user
        currentUser.password = null;
        return done(null, currentUser);
        } catch (error) {
            return done(error);
        }
    }
);

//Función que utiliza el usuario de req.LogIn para registrar su id.
passport.serializeUser((user, done) => {
    return done(null, user._id);
});

//Función que busca un usuario según el _id en la DB y populará req.user si existe
passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser)
    } catch (err) {
        return done(err);
    }
});

passport.use('login', loginStrategy);

module.exports = loginStrategy;