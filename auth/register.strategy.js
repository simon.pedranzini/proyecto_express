// Estrategia de registro
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User')
const bcrypt = require('bcrypt')

const registerStrategy = new LocalStrategy(
    {
        usernameField: 'email', // cual es el campo que contiene el nombre de usuario
        passwordField: 'password', // cual es el campo que contiene la contraseña
        passReqToCallback: true, // pasarle la request al callback. Si es false, no se pasa.
    },
    async (req, email, password, done) => {
        try {
        // 1. Comprobar que el usuario no exista previamente.
        const existingUser = await User.find({ email : email });
        // Si no existe =>
        if (existingUser.length) {
            const error = new Error('Este usuario ya está registrado');
            return done(error);
        }
        // 2. Encriptar la contraseña y guardarla en una variable
        const saltRounds = 10 // saltRound las veces que se ecnrypta las pass => 10 veces se encrpypta
        const hash = await bcrypt.hash(password, saltRounds);

        // 3. Creamos el usuario y lo guardamo en la base de datos
        const newUser = new User({
            email: email,
            password: hash,
            name: req.body.name
        })
        const savedUser = await newUser.save();

        return done(null, savedUser); // El primer parametro es el error, por lo que si se guarda el usuario el error es null.

    } catch (error) {
        done(error, null); // El segundo valor es null porque ha dado fallo y no hay usuario para mostrar.
    }

        // 4. Iniciamos sesión al usuario llamando a passport.authenticate()

    });

passport.use('registrito', registerStrategy) // Cuando alguien llame registrito ejecuta registerStrategy


module.exports = registerStrategy;